module main

import irishgreencitrus.raylibv as r
import game

fn main() {
	// initialise game
	mut g := game.Game {}
	g.load_game()

	r.set_config_flags(r.flag_window_resizable | r.flag_msaa_4x_hint)
	r.init_window(1280, 720, g.meta.name.str)
	r.set_target_fps(60)

	$if prod {
		r.set_exit_key(r.key_null)
		r.set_trace_log_level(r.log_warning)
	}

	g.start()

	// main loop
	for !r.window_should_close() {
		r.begin_drawing()
		r.clear_background(r.black)

		delta := r.get_frame_time()

		g.update(delta)

		r.end_drawing()
	}

	r.close_window()
}
