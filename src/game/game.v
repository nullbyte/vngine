module game

import os
import loader
import saves

const (
	game_path = 'game'
	save_dir_name = 'vengine'
)

pub struct Game {
pub mut:
	meta GameMeta
	game_path string
	scene Scene
}

pub struct GameMeta {
pub:
	name string
	start_script string
	characters_folder string
	backgrounds_folder string
}

pub fn (mut g Game) start() {
	g.scene = Scene { meta: g.meta }
	g.scene.game_path = g.game_path
	g.scene.start(os.join_path(game_path, g.meta.start_script))

	g.scene.onsave = g.save
	g.scene.onload = g.load
}

pub fn (mut g Game) load_game() {
	g.meta = loader.load_game[GameMeta](game_path)
	g.game_path = game_path
}

pub fn (mut g Game) update(delta f32) {
	g.scene.process(delta)
	g.scene.render(delta)
}

pub fn (g Game) load() {
	mut save_path := os.config_dir() or { os.join_path(os.getenv('HOME'), '.config') }

	save_path = os.join_path(save_path, save_dir_name, g.meta.name)
	println(save_path)
}

pub fn (g Game) save() {
	mut save_path := os.config_dir() or { os.join_path(os.getenv('HOME'), '.config') }

	save_path = os.join_path(save_path, save_dir_name, g.meta.name)

	save := saves.Save { current_line: g.scene.current_line, current_scene: g.scene.scene_path }
	save.write(save_path)
}
