module screen

import irishgreencitrus.raylibv as r

// virtual screen dimensions
pub const (
	width = 1920
	height = 1080
)

// translates in-engine units into on-screen pixels
pub fn rect(x int, y int, w int, h int) r.Rectangle {
	width_scale := r.get_screen_width() / f32(width)
	height_scale := r.get_screen_height() / f32(height)
	
	mut width_offset := ((width_scale - height_scale) / 2) * width
	mut height_offset := ((height_scale - width_scale) / 2) * height

	if width_offset < 0 {
		width_offset = 0
	}
	if height_offset < 0 {
		height_offset = 0
	}

	mut scale := f32(0)

	if width_scale < height_scale {
		scale = width_scale
	} else {
		scale = height_scale
	}

	return r.Rectangle { (x * scale) + width_offset, (y * scale) + height_offset, w * scale, h * scale }
}

// translates in-engine units into on-screen pixels
pub fn pos(x int, y int) r.Vector2 {
	width_scale := r.get_screen_width() / f32(width)
	height_scale := r.get_screen_height() / f32(height)
	
	mut width_offset := ((width_scale - height_scale) / 2) * width
	mut height_offset := ((height_scale - width_scale) / 2) * height

	if width_offset < 0 {
		width_offset = 0
	}
	if height_offset < 0 {
		height_offset = 0
	}

	mut scale := f32(0)

	if width_scale < height_scale {
		scale = width_scale
	} else {
		scale = height_scale
	}

	return r.Vector2 { (x * scale) + width_offset, (y * scale) + height_offset }
}

// translates in-engine units into on-screen pixels
pub fn size(x int, y int) r.Vector2 {
	width_scale := r.get_screen_width() / f32(width)
	height_scale := r.get_screen_height() / f32(height)

	mut scale := f32(0)

	if width_scale < height_scale {
		scale = width_scale
	} else {
		scale = height_scale
	}

	return r.Vector2 { x * scale, y * scale }
}

// scales a unit to the window size
pub fn scale(unit int) f32 {
	width_scale := r.get_screen_width() / f32(width)
	height_scale := r.get_screen_height() / f32(height)

	mut scale := f32(0)

	if width_scale < height_scale {
		scale = width_scale
	} else {
		scale = height_scale
	}

	return unit * scale
}

pub fn screen_rect() r.Rectangle {
	return rect(0, 0, screen.width, screen.height)
}

// scales and crops a rectangle to fill the screen
// pub fn scale_crop_rect(width f32, height f32) {
// 	width_scale := r.get_screen_width() / f32(width)
// 	height_scale := r.get_screen_height() / f32(height)

// 	mut result := r.Rectangle {x: 0, y: 0}

// 	if width_scale < height_scale {
// 		result.width = width_scale
// 		result.height = width_scale
// 	} else {
// 		result.width = height_scale
// 		result.height = height_scale
// 	}

// 	return result
// }