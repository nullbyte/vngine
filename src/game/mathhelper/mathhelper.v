module mathhelper

pub fn lerp(to f32, from f32, weight f32) f32 {
	return (to - from) * (1 - weight) + from
}
