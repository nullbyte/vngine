module game

import os
import irishgreencitrus.raylibv as r
import character
import animation
import screen
import menu

const (
	dialog_height = 200
	dialog_margin = 10
	font_size = 32
)

pub struct Scene {
pub:
	meta GameMeta
pub mut:
	scene_path string
	script []string
	
	game_path string
	current_line int
	text_display_progress f32 = 1
	
	background r.Texture2D
	characters map[string]&character.Character
	
	line_animations []animation.Animation // Animations that only last for the length of the current line
	blocking_animations []animation.Animation // Animations that will count as a line and block a progression prompt (click)
	animations []animation.Animation // Animations that run regardless of the line
	nopause_animations []animation.Animation // Animations that will even when the game is paused
	
	paused bool
	pause_menu menu.Menu

	onsave fn ()
	onload fn ()
}

pub fn (mut s Scene) start(path string) {
	s.scene_path = path
	
	s.script = os.read_lines(path) or {
		eprintln('Cannot find file ${path}')
		exit(1)
	}

	s.pause_menu.onclose = fn [mut s] () { s.resume() }
	s.pause_menu.onsave = fn [mut s] () { s.onsave() }
	s.pause_menu.onload = fn [mut s] () { s.onload() }
	s.pause_menu.init()

	s.process_line()
}

pub fn (mut s Scene) reset() {
	s.current_line = 0
	s.characters.clear()
	s.characters = map[string]&character.Character
	s.animations.clear()
	s.line_animations.clear()
	s.blocking_animations.clear()
	s.nopause_animations.clear()
}

pub fn (mut s Scene) goto_line(line int) {
	s.reset()
	for s.current_line < line {
		s.next_line() or { break }
	}
}

pub fn (mut s Scene) render(delta f32) {
	s.render_background()
	s.render_characters()
	s.render_dialog()
	s.render_borders()

	if r.is_key_pressed(r.key_space) {
		s.goto_line(8)
	}

	if s.pause_menu.opacity > 0 {
		s.pause_menu.render(delta)
	}
}

fn (mut s Scene) render_background() {
	// Get rectangle to display from background image
	mut source_rect := r.Rectangle {}

	screen_aspect := f32(screen.width) / f32(screen.height)

	if screen_aspect > 1 {
		cropped_width := s.background.height * screen_aspect
		source_rect.width = cropped_width
		source_rect.height = s.background.height
		source_rect.x = (s.background.width - cropped_width) / 2
	} else {
		cropped_height := s.background.width * screen_aspect
		source_rect.height = cropped_height
		source_rect.width = s.background.width
		source_rect.y = (s.background.height - cropped_height) / 2
	}

	r.draw_texture_pro(s.background, source_rect, screen.screen_rect(), r.Vector2 {0, 0}, 0, r.white)
}

fn (mut s Scene) render_characters() {
	for _, c in s.characters {
		c.sprite.draw_centered(c.pos, c.rotation, c.scale)
		// r.draw_texture_ex(
		// 	c.sprite,
		// 	c_pos,
		// 	c.rotation,
		// 	f32(screen.scale(1)) * c.scale,
		// 	r.Color { 255, 255, 255, u8(255 * c.opacity) }
		// )
	}
}

// Display current speech in the on-screen dialog
fn (mut s Scene) render_dialog() {
	// Background
	background_pos := screen.rect(dialog_margin, screen.height - dialog_height - dialog_margin, screen.width - (dialog_margin * 2), dialog_height)

	r.draw_rectangle_rec(
		background_pos,
		r.Color { 10, 10, 10, 80 }
	)

	// Text
	if s.script[s.current_line].starts_with('[') { return }
	text_pos := screen.pos(dialog_margin * 2, screen.height - dialog_height)

	// Text wrapping
	script_line := s.script[s.current_line].substr(0, int(s.text_display_progress * s.script[s.current_line].len))
	script_words := script_line.split(' ')

	mut display_line := ''
	mut current_word := 0
	mut lines_displayed := 0

	for {
		if current_word >= script_words.len {
			// End of text, display and exit
			r.draw_text(
				display_line.str,
				int(text_pos.x),
				int(text_pos.y + screen.scale(lines_displayed * font_size + 10)),
				int(screen.scale(font_size)),
				r.white
			)

			break
		}

		display_line_with_next_word := display_line + script_words[current_word]

		if r.measure_text(display_line_with_next_word.str, int(screen.scale(font_size))) < int(screen.scale(1800)) {
			display_line = display_line_with_next_word + ' '
			current_word++
		} else {
			// No more space, display line and move to the next one
			r.draw_text(
				display_line.str,
				int(text_pos.x),
				int(text_pos.y + screen.scale(lines_displayed * font_size + 10)),
				int(screen.scale(font_size)),
				r.white
			)

			display_line = ''
			lines_displayed++
		}
	}
}

// Draw black rectangles over out-of-bounds areas
fn (mut s Scene) render_borders() {
	width_scale := r.get_screen_width() / f32(screen.width)
	height_scale := r.get_screen_height() / f32(screen.height)

	mut scale := f32(0)

	if width_scale < height_scale {
		scale = width_scale
	} else {
		scale = height_scale
	}

	width_offset := ((width_scale - height_scale) / 2) * screen.width
	height_offset := ((height_scale - width_scale) / 2) * screen.height

	r.draw_rectangle(0, 0, int(width_offset), r.get_screen_height(), r.black)
	r.draw_rectangle(0, 0, r.get_screen_width(), int(height_offset), r.black)
	r.draw_rectangle(int(screen.width * scale + width_offset), 0, int(width_offset), r.get_screen_height(), r.black)
	r.draw_rectangle(0, int(screen.height * scale + height_offset), r.get_screen_width(), int(height_offset), r.black)
}

// When the player clicks to advance the story
pub fn (mut s Scene) next_line()! {
	// Check for blocking animations
	if s.blocking_animations.len > 0 {
		for i, mut a in s.blocking_animations {
			a.skip()
			s.blocking_animations.delete(i)
		}

		return
	}

	s.current_line++

	if s.current_line >= s.script.len {
		return error('Script end')
	}

	s.process_line()

	// End all line-animations
	for i, mut a in s.line_animations {
		a.skip()
		s.line_animations.delete(i)
	}

	// Create new line animation for text progression
	s.text_display_progress = 0
	mut text_anim := animation.Animation { target: &s.text_display_progress }
	text_anim.duration = s.script[s.current_line].len / 20
	text_anim.start()
	s.blocking_animations << text_anim
}

// Update all active animations in the game loop
pub fn (mut s Scene) process(delta f32) {
	if !s.paused && should_progress() {
		s.next_line() or {
			exit(0)
		}
	}

	if should_pause() {
		if s.paused {
			s.resume()
		} else {
			s.pause()
		}
	}

	if s.paused {
		s.pause_menu.process(delta)
	}

	s.update_animations(delta)
}

fn (mut s Scene) pause() {
	s.paused = true
	s.nopause_animations << s.pause_menu.show()
}

fn (mut s Scene) resume() {
	s.paused = false
	s.nopause_animations << s.pause_menu.hide()
}

fn (mut s Scene) update_animations(delta f32) {
	for i, mut anim in s.nopause_animations {
		if anim.is_complete() {
			s.nopause_animations.delete(i)
		} else {
			anim.move(delta)
		}
	}

	if s.paused { return }
	
	// Update animations
	for i, mut anim in s.animations {
		if anim.is_complete() {
			s.animations.delete(i)
		} else {
			anim.move(delta)
		}
	}

	// Update line animations
	for i, mut anim in s.line_animations {
		if anim.is_complete() {
			s.line_animations.delete(i)
		} else {
			anim.move(delta)
		}
	}

	// Update blocking animations
	for i, mut anim in s.blocking_animations {
		if anim.is_complete() {
			s.blocking_animations.delete(i)
		} else {
			anim.move(delta)
		}
	}
}

fn should_progress() bool {
	return  r.is_key_pressed(r.key_space) ||
	        r.is_key_pressed(r.key_enter) ||
	        r.is_mouse_button_pressed(r.mouse_button_left)
}

fn should_pause() bool {
	return  r.is_key_pressed(r.key_escape) ||
	        r.is_mouse_button_pressed(r.mouse_button_right)
}