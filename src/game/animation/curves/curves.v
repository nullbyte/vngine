module curves

import math

pub const (
	curve = {
		'linear': linear
		'expo': expo
		'outexpo': outexpo
		'cubic': cubic
		'jump': jump
	}
)

fn linear(x f32) f32 {
	return x
}

fn expo(x f32) f32 {
	return f32(math.pow(x, 2))
}

fn outexpo(x f32) f32 {
	return f32(-math.pow(1 - x, 2) + 1)
}

fn cubic(x f32) f32 {
	return f32(math.pow(x, 3))
}

fn jump(x f32) f32 {
	return f32(-math.pow((1 - 2*x), 2) + 1)
}