module controls

import irishgreencitrus.raylibv as r
import mathhelper
import screen
import math

pub struct Button {
mut:
	hovered_size_increase f32
	clicked bool
pub mut:
	pos r.Vector2
	size r.Vector2
	text string
	font_size int = 32
	color r.Color = r.white
	border_color r.Color = r.gray
	font_color r.Color = r.black
	border_size int = 5
	onclick ?fn ()
}

pub fn (b Button) render(delta f32) {
	// Calculate dimensions for drawing
	// math.round is needed to prevent rounding errors in f32 to int conversion
	rect := screen.rect(int(math.round(b.pos.x)), int(math.round(b.pos.y)), int(math.round(b.size.x)), int(math.round(b.size.y)))

	bs := screen.scale(int(math.round(b.border_size + b.border_size * b.hovered_size_increase)))
	border_rect := screen.rect(int(math.round(b.pos.x - bs)), int(math.round(b.pos.y - bs)), int(math.round(b.size.x + bs * 2)), int(math.round(b.size.y + bs * 2)))
	
	clickfade := if b.clicked { 0.8 } else { 1 }
	color := r.Color { u8(b.color.r * clickfade), u8(b.color.g * clickfade), u8(b.color.b * clickfade), b.color.a }
	
	r.draw_rectangle_rounded(border_rect, 0.5, 4, b.border_color)
	r.draw_rectangle_rounded(rect, 0.5, 4, color)
	
	// text
	middle := screen.pos(int(b.pos.x + b.size.x / 2), int(b.pos.y + b.size.y / 2))
	text_size := r.measure_text_ex(r.get_font_default(), b.text.str, screen.scale(b.font_size), screen.scale(b.font_size / 10))

	r.draw_text_ex(r.get_font_default(), b.text.str, r.Vector2 { middle.x - text_size.x / 2, middle.y - text_size.y / 2 }, screen.scale(b.font_size), screen.scale(b.font_size / 10), b.font_color)
}

pub fn (mut b Button) process(delta f32) {
	is_hovered := r.check_collision_point_rec(r.get_mouse_position(), screen.rect(int(b.pos.x), int(b.pos.y), int(b.size.x), int(b.size.y)))
	
	b.hovered_size_increase = mathhelper.lerp(b.hovered_size_increase, int(is_hovered), delta * 16)

	if is_hovered &&  r.is_mouse_button_down(r.mouse_button_left) {
		if !b.clicked {
			b.clicked = true
			if onclick := b.onclick {
				onclick()
			}
		}
	} else {
		b.clicked = false
	}
}