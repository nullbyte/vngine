module controls

import irishgreencitrus.raylibv as r
import screen
import math

pub struct Save {
	btn_width int = 120
	btn_margin int = 20
mut:
	ui []Control
pub mut:
	pos r.Vector2
	size r.Vector2
	name string
	savepath string
	onsave ?fn ()
}

pub fn (mut s Save) show(savepath string) {
	s.savepath = savepath

	middle := int(s.pos.y + s.size.y / 2)
	text_size := r.measure_text_ex(r.get_font_default(), s.name.str, screen.scale(32), screen.scale(32 / 10))
	
	s.ui << Label { text: s.name, pos: r.Vector2 { s.pos.x + 10, middle - text_size.y / 2 }, color: r.black }
	s.ui << Button { text: 'Save', pos: r.Vector2 { s.pos.x + s.size.x - (s.btn_width + s.btn_margin) * 3, s.pos.y + s.btn_margin }, size: r.Vector2 { s.btn_width, s.size.y - s.btn_margin * 2 } }
	s.ui << Button { text: 'Load', pos: r.Vector2 { s.pos.x + s.size.x - (s.btn_width + s.btn_margin) * 2, s.pos.y + s.btn_margin }, size: r.Vector2 { s.btn_width, s.size.y - s.btn_margin * 2 } }
	s.ui << Button { text: 'Delete', pos: r.Vector2 { s.pos.x + s.size.x - (s.btn_width + s.btn_margin), s.pos.y + s.btn_margin }, size: r.Vector2 { s.btn_width, s.size.y - s.btn_margin * 2 } }
}

pub fn (s Save) render(delta f32) {
	// Calculate dimensions for drawing
	// math.round is needed to prevent rounding errors in f32 to int conversion
	rect := screen.rect(int(math.round(s.pos.x)), int(math.round(s.pos.y)), int(math.round(s.size.x)), int(math.round(s.size.y)))
	
	r.draw_rectangle_rounded(rect, 0.5, 4, r.white)
	
	for c in s.ui {
		c.render(delta)
	}
}

pub fn (mut s Save) process(delta f32) {
	for mut c in s.ui {
		c.process(delta)
	}
}