module controls

import irishgreencitrus.raylibv as r
import screen

pub struct Label {
pub mut:
	pos r.Vector2
	size r.Vector2
	text string
	font_size int = 32
	color r.Color = r.white
}

pub fn (l Label) render(delta f32) {
	r.draw_text_ex(r.get_font_default(), l.text.str, screen.pos(int(l.pos.x), int(l.pos.y)), screen.scale(l.font_size), screen.scale(l.font_size / 10), l.color)
}

pub fn (l Label) process(delta f32) {
	
}