module menu

import irishgreencitrus.raylibv as r
import animation
import animation.curves
import controls
import screen

pub struct Menu {
mut:
	active bool
pub mut:
	ui []controls.Control
	
	opacity f32
	
	onclose fn ()
	onsave fn ()
	onload fn ()
}

pub fn (mut m Menu) init() {
	m.ui << controls.Label { text: "Paused", pos: r.Vector2 { 50, 50 }, font_size: 128 }
	
	m.ui << controls.Button { text: "Resume", pos: r.Vector2 { 50, 680 }, size: r.Vector2 { 400, 50 }, onclick: m.onclose }
	m.ui << controls.Button { text: "Saves", pos: r.Vector2 { 50, 780 }, size: r.Vector2 { 400, 50 }, onclick: fn [mut m] () { m.show_page(.save) } }
	m.ui << controls.Button { text: "Settings", pos: r.Vector2 { 50, 880 }, size: r.Vector2 { 400, 50 }, onclick: fn [mut m] () { m.show_page(.settings) } }
	m.ui << controls.Button { text: "Quit", pos: r.Vector2 { 50, 980 }, size: r.Vector2 { 400, 50 }, onclick: fn () { r.close_window() exit(0) } }

	mut save := controls.Save { pos: r.Vector2 { 500, 500 }, size: r.Vector2 { 1200, 80 }, name: "Test save" }
	save.show('test')
	m.ui << save
}

pub fn (mut m Menu) show() animation.Animation {
	m.active = true
	mut show_anim := animation.Animation { target: &m.opacity, duration: 0.2, to: 1, curve: curves.curve['expo'] }
	show_anim.start()
	return show_anim
}

pub fn (mut m Menu) hide() animation.Animation {
	m.active = false
	mut hide_anim := animation.Animation { target: &m.opacity, duration: 0.2, to: 0, curve: curves.curve['expo'] }
	hide_anim.start()
	return hide_anim
}

pub fn (mut m Menu) show_page(page Page) {
	m.hide_submenus()
	match page {
		.settings {
			println('settings')
		}
		.save {
			m.show_saves()
		}
	}
}

enum Page {
	save
	settings
}

pub fn (mut m Menu) render(delta f32) {
	r.draw_rectangle_rec(screen.screen_rect(), r.Color { 0, 0, 0, u8(200 * m.opacity) })
	
	if !m.active { return }

	for control in m.ui {
		control.render(delta)
	}
}

pub fn (mut m Menu) process(delta f32) {
	for mut control in m.ui {
		control.process(delta)
	}
}

fn (mut m Menu) show_saves() {
}

fn (mut m Menu) hide_submenus() {
	m.hide_settings()
	m.hide_saves()
}

fn (mut m Menu) hide_settings() {
	for i, s in m.ui {
		if s is controls.Save {
			m.ui.delete(i)
		}
	}
}

fn (mut m Menu) hide_saves() {
	for i, s in m.ui {
		if s is controls.Save {
			m.ui.delete(i)
		}
	}
}