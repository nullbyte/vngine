run:
	v -cc clang run .

publish:
	v -cc clang -cflags "-O3 -flto" -prod .
	strip vngine

windows:
	v -cc clang -cflags "-O3 -flto --static" -os windows -prod .

check:
	v -check .