## VNgine

A Visual Novel engine written in V.
A sample game has been included in the `game` folder. The script there should be enough to get started writing a story. To distribute, just put the executable next to the game folder and send in a zip.